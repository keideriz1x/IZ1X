<?php
if (isset($_SESSION)) {
  $nombre = $_SESSION["datos"]->nombre;
  $apellido = $_SESSION["datos"]->apellido;
}

?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/admin/permisoAdmin.php") ?>&priv=<?php echo base64_encode("presentacion/admin/sesionAdministrador.php") ?>"><img src="img/logo.png" width="100px"></a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Producto
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("crearProducto")) ?>">Crear</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("consultarProducto")) ?>">Consultar</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria y Tipo
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("ccCategoria")) ?>">Categoria</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("ccTipo")) ?>">Tipo</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cliente
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("consultarCliente")) ?>">Consultar</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Domiciliario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("crearDomiciliario")) ?>">Crear</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("consultarDomiciliario")) ?>">Consultar</a>
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("log")) ?>">Log</a>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pedidos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("consultarPedido")) ?>">Consultar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("validarPedido")) ?>">Validar</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item active d-none d-lg-none d-xl-block my-auto">

        <?php  
        if($_SESSION["datos"]->foto==null)
        {
          ?>
           <i class="fas fa-user-circle fa-2x pt-1" style="color: white;">  
          <?php  
        }else{
          ?>
                  <img src="data:image/png;base64,<?php echo $_SESSION["datos"]->foto?>" class="menuPerfil mx-auto d-block " >
          <?php  
        }
        ?>
         </i>        <!-- Foto aqui, tamaño de foto de 2em -->
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Administrador: <?php echo " " . $nombre . " " . $apellido ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/admin/permisoAdmin.php") ?>&priv=<?php echo base64_encode("presentacion/editPerfil.php") ?>">Editar Perfil</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("servicios/service_sesion/logout.php")?>">Cerrar Sesión</a>
        </div>
      </li>
    </ul>
  </div>

</nav>