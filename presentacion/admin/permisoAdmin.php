<?php
if (isset($_SESSION["id"])) {
    if ($_SESSION["rol"] == "Administrador") {
        
        switch (base64_decode($_GET["priv"])) {
            case "presentacion/editPerfil.php":
                include "presentacion/editPerfil.php";
                break;
            case "presentacion/admin/sesionAdministrador.php":
                include "presentacion/admin/sesionAdministrador.php";
                break;
            default:
            include "presentacion/noExiste.php";
            break;
        }
    
    ?>
    <?php
    }elseif ($_SESSION["rol"] == "Cliente" || $_SESSION["rol"] == "Domiciliario") {
        if ($_SESSION["rol"] == "Cliente") {
            include "presentacion/permisos.php";
        }
        elseif ($_SESSION["rol"] == "Domiciliario") {
            include "presentacion/permisos.php";
        }
    }
?>
<?php
}else{
    header("Location: index.php");
}
?>