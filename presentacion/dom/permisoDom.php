<?php
if (isset($_SESSION["id"])) {
    if ($_SESSION["rol"] == "Domiciliario") {
        switch (base64_decode($_GET["priv"])) {
            case "presentacion/dom/sesionDomiciliario.php":
                include "presentacion/dom/sesionDomiciliario.php";
                break;
            case "presentacion/editPerfil.php":
                include "presentacion/editPerfil.php";
                break;
            default:
            include "presentacion/noExiste.php";
            break;
        }
    
    ?>
    <?php
    }elseif ($_SESSION["rol"] == "Cliente" || $_SESSION["rol"] == "Administrador") {
        if ($_SESSION["rol"] == "Cliente") {
            include "presentacion/permisos.php";
        }
        elseif ($_SESSION["rol"] == "administrador") {
            include "presentacion/permisos.php";
        }
    }
?>
<?php
}else{
    header("Location: index.php");
}
?>