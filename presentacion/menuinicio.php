<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="col-3 order-0 order-lg-0 order-md-0 d-flex justify-content-lg-center">
    <div class="my-auto"><button class="navbar-toggler px-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button></div>
    <div class="my-auto"><a class="navbar-brand" href="index.php"><img src="img/logo.png" style="width: 100px;"></a></div>
  </div>
  <div class="col-lg-6 order-2 order-lg-1 order-md-2">
    <div class="mx-auto d-block w-100">
      <div class="input-group">
        <div class="input-group-prepend">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span style="font-size: calc(0.9% + 0.9em)">Categoria</span>
          </button>

          <ul class="dropdown-menu" role="menu">

          </ul>
        </div>
        <input type="text" class="form-control" aria-label="Text input with dropdown button">
        <span class="input-group-prepend">
          <button class="btn btn-primary" type="button"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
              <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
            </svg></button>
        </span>
      </div>
    </div>
  </div>
  <div class="col-3 order-1 order-lg-2 order-md-1 d-inline-flex justify-content-center">
    <?php
    if (isset($_SESSION["rol"])) { ?>
      <a class="d-flex text-white text-decoration-none">
        <img src="data:image/png;base64,<?php echo $_SESSION["datos"]->foto ?>" class="menuPerfil my-auto ">
        <div class=" ml-2 my-auto d-none d-md-none d-lg-block text-nowrap"><?php echo $_SESSION["datos"]->nombre . " " . $_SESSION["datos"]->apellido; ?></div>
      </a>
    <?php
    } else { ?>
      <a href="?" data-toggle="modal" data-target="#exampleModal" class="d-flex text-white text-decoration-none">
        <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" fill="currentColor" class="bi bi-person my-auto" viewBox="0 0 16 16">
          <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
        </svg>
        <div class=" ml-2 my-auto d-none d-md-none d-lg-block text-nowrap">Mi cuenta</div>
      </a>
    <?php
    }
    ?>
    <hr style="border-left: 2px dotted #00CCFF;height:40px;width:1px" class="mx-1">
    <a href="index.php?pid=<?php echo base64_encode("presentacion/client/permisoClient.php") ?>&priv=<?php echo base64_encode("presentacion/client/car.php") ?>" class=" d-flex text-white text-decoration-none">
      <svg xmlns="http://www.w3.org/2000/svg" style="border-radius: 8px;z-index: 1;" width="2em" height="2em" fill="currentColor" class="bi bi-cart3 my-auto position-relative overflow-hidden" viewBox="0 0 16 16">
        <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
      </svg>
      <span class="text-white text-nowrap text-center position-relative" style="font-size: 10px;line-height: 20px;border-radius: 50%;background-color: #00CCFF;-webkit-box-sizing: border-box;box-sizing: border-box;min-width: 20px;height: 20px;padding: 0 0.5833333333em;right: 1em;top: 1.5em;z-index: 3;">5</span>
    </a>
  </div>
</nav>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
    <div class="text-white mx-4">PS5</div>
    <div class="text-white mx-4">PS4</div>
    <div class="text-white mx-4">XBOX ONE</div>
    <div class="text-white mx-4">NINTENDO SWITCH</div>
    <?php
    if (isset($_SESSION["rol"])) { ?>
      <div class="mx-4 "><a class="text-decoration-none text-white" href="index.php?pid=<?php echo base64_encode("presentacion/client/permisoClient.php") ?>&priv=<?php echo base64_encode("presentacion/client/pedido.php") ?>"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-handbag-fill pb-1 d-none d-md-none d-lg-inline" viewBox="0 0 16 16">
  <path d="M8 1a2 2 0 0 0-2 2v2H5V3a3 3 0 0 1 6 0v2h-1V3a2 2 0 0 0-2-2zM5 5H3.361a1.5 1.5 0 0 0-1.483 1.277L.85 13.13A2.5 2.5 0 0 0 3.322 16h9.356a2.5 2.5 0 0 0 2.472-2.87l-1.028-6.853A1.5 1.5 0 0 0 12.64 5H11v1.5a.5.5 0 0 1-1 0V5H6v1.5a.5.5 0 0 1-1 0V5z"/>
</svg><b>PEDIDOS</b></a></div>
      <ul class="navbar-nav mx-4">
        <li class="nav-item dropdown active">
          <a class="nav-link dropdown-toggle p-0 text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-person-square mb-1 d-none d-md-none d-lg-inline" viewBox="0 0 16 16">
              <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
              <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm12 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1v-1c0-1-1-4-6-4s-6 3-6 4v1a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12z" />
            </svg> <b>PERFIL</b>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/client/permisoClient.php") ?>&priv=<?php echo base64_encode("presentacion/editPerfil.php") ?>">Editar Perfil</a>
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("servicios/service_sesion/logout.php") ?>">Cerrar Sesión</a>
          </div>
        </li>
      </ul>
    <?php
    } ?>
  </div>
</nav>

<div class="modal fade pt-5" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header d-block">
        <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title text-center" id="exampleModalLabel">Iniciar Sesión</h5>
      </div>
      <div class="modal-body">
        <form action=<?php echo "index.php?pid=" . base64_encode("servicios/service_sesion/logear.php") ?> method="post">
          <div class="form-group">
            <input type="email" name="correo" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" class="form-control" placeholder="Correo" required="required">
          </div>
          <div class="form-group">
            <input type="password" name="clave" minlength="4" maxlength="15" class="form-control" placeholder="Clave" required="required">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-outline-primary btn-block">Autenticar</button>
          </div>
          <p class="text-center">
            <a href="<?php echo "index.php?pid=" . base64_encode("presentacion/excepciones.php") . "&priv=" . base64_encode("presentacion/client/clave.php") ?>">¿Olvido su contraseña?</a>
          </p>
          <p class="text-center">
            <a>o</a>
          </p>
          <p class="text-center">
            <a href="<?php echo "index.php?pid=" . base64_encode("presentacion/excepciones.php") . "&priv=" . base64_encode("presentacion/client/crear.php") ?>">Crear una cuenta</a>
          </p>
        </form>
      </div>
    </div>
  </div>
</div>