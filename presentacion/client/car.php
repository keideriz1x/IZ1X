

<!-- <script src="app/presentacion/producto/carrito.js"></script> -->
<?php
if(!isset($_SESSION))
session_start();


?>

<div class='container'>
        <div class='row mt-4'>
    
            <div class='col-lg-12'>
                <div class='card pt-4'>
                    <div class='cardCl card-header text-center text-white rounded'>
                       <h3>Carrito de compras</h3>
                    </div>
                 
                    <div class='card-body'>
                        <div class='contenedor table-responsive' id ="productos_carrito">
    
                        </div>
                      
                        <div class='w-100 pt-3'>
                            <a type='button' class='btn btn-primary col-lg-2'  onclick='history.back()'>Regresar</a>
                         
                            <a type='button' id="continuar" href="index.php?pid=<?php echo base64_encode("presentacion/client/permisoClient.php") ?>&priv=<?php echo base64_encode("presentacion/client/compra.php")?>" class='btn btn-danger float-right col-lg-2' >Continuar</a>
                           
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
    </div>

    <?php
   
    ?>