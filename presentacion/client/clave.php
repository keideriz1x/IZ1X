<?php
include "presentacion/menuinicio.php";

?>
<div class="col-lg-5 container pt-5">
    <div class="card">
        <div class="cardCl card-header text-center text-white rounded">
            Recupera tu cuenta
        </div>
        <div class="card-body text-center">
        <form  id="clave" action=<?php echo "index.php?pid=" . base64_encode("app/presentacion/clave.php") ?> method="post">
        <div class="pb-2">Ingresa tu correo electrónico para enviar codigo de cambio de contraseña</div>
						<div class="form-group">
							<input type="email" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" id="correo" class="form-control" placeholder="Correo electronico" required="required" autocomplete="off">
						</div>
						<div class="form-group text-center">
					<button type="submit" id="enviar" class="btn btn-primary btn-block">Enviar</button>
				</div>
				</form>
        </div>
    </div>
</div>