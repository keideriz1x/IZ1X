<?php
if (isset($_SESSION["id"])) {
    if ($_SESSION["rol"] == "Cliente") {
        switch (base64_decode($_GET["priv"])) {
            case "presentacion/client/car.php":
                include "presentacion/client/car.php";
                break;
            case "presentacion/editPerfil.php":
                include "presentacion/editPerfil.php";
                break;
            case "presentacion/client/pedido.php":
                include "presentacion/client/pedido.php";
                break;
            case "presentacion/client/compra.php":
                include "presentacion/client/compra.php";
                break;
            default:
            include "presentacion/noExiste.php";
            break;
        }
    ?>
    <?php
    }elseif ($_SESSION["rol"] == "Administrador" || $_SESSION["rol"] == "Domiciliario") {
        if ($_SESSION["rol"] == "Administrador") {
            include "presentacion/permisos.php";
        }
        elseif ($_SESSION["rol"] == "Domiciliario") {
            include "presentacion/permisos.php";
        }
        
    }
?>
<?php
}else{
    include "presentacion/sinSesion.php";
}
?>
