<?php
include "presentacion/menuinicio.php";
?>
<div class='col-lg-4 container pt-5'>
    <div class='row mt-4'>

        <div class='col-lg-12'>
            <div class='card'>
                <div class='card-body py-5'>
                    <h2 class='text-center'>¡Hola! Para acceder, ingresa a tu cuenta</h2>
                    <div class='text-center pt-3'>
                        <a class='text-white py-3 px-4' href='<?php echo "index.php?pid=" . base64_encode("presentacion/excepciones.php") . "&priv=" . base64_encode("presentacion/client/crear.php") ?>' style='text-decoration:none;background:#000000'>Soy nuevo</a>
                    </div>
                    <div class='text-center pt-4'>
                    
                        <a href='#' data-toggle='modal' data-target='#exampleModal'>Ya tengo cuenta</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>