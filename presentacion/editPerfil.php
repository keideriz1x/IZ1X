<script src="servicios\js\editar-perfil.js"></script>

<div class="container">
    <div class="row">
        <div class="col-lg-6 mx-auto pt-3">
            <div class="card pt-4">
                <div class="cardCl card-header text-center text-white rounded">
                    <h3>Editar Perfil</h3>
                </div>
                <div class="card-body">
                    <form id="edit" enctype="multipart/form-data" action=<?php echo "index.php?pid=" . base64_encode("app/presentacion/permisoCliente.php") . "&priv=" . base64_encode(base64_encode("perfilCliente")) ?> method="post">
                        <div class="input-group mb-3 pt-3">
                            <div class="container">
                                <?php
                                if ($_SESSION["datos"]->foto == null) {
                                ?>
                                    <img src="app/img/profile.jpeg" class="imgPerfil mx-auto d-block">
                                <?php
                                } else {
                                ?>
                                    <div id="foto" class="imgPerfil mx-auto d-block"></div>
                                <?php
                                }
                                ?>

                                <div class="row pt-3 centerdiv">
                                    <div class="col-sm-6 px-3"><label for="files" class="w-100 btn btn-primary ">Seleccionar Foto</label></div>
                                    <div class="col-sm-6 px-3"><input placeholder="Elegir foto" autocomplete="off" id="m1" class="w-100 read form-control" />
                                        <input id="files" type="file" class="inputFile" onChange="actualiza(this.files[0].name)">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="id" value="<?php echo $_SESSION["id"] ?>">
                        <input type="hidden" id="rol" value="<?php echo $_SESSION["rol"] ?>">
                        <div class="form-group">
                            <input type="text" id="nombre" value="" class="form-control" placeholder="Nombre" required="required" autocomplete="off" maxlength="20" minlength="3">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="apellido" value="" placeholder="Apellido" required="required" autocomplete="off" maxlength="20" minlength="3">
                        </div>
                        <div class="form-group">
                            <input type="text" class="validanumericos form-control" value="" id="correo" placeholder="Correo" autocomplete="off" readonly>
                        </div>
                        <?php 
                        if (isset($_SESSION["id"]) && $_SESSION["rol"] == "Cliente") {?>
                                    <div class="form-group">
                                    <input type="text" class="validanumericos form-control" value="" id="direccion" placeholder="Direccion" autocomplete="off" maxlength="40" minlength="15">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="validanumericos form-control" value="" id="telefono" placeholder="Telefono" autocomplete="off" maxlength="10"  minlength="8">
                                </div>
                       <?php } 
                        ?>
                        <!-- <div class="form-group">
                            <input type="text" class="validanumericos form-control" value="" id="clave" placeholder="Clave" required="required" autocomplete="off">
                        </div> -->
                        <div class="form-group text-center">
                            <button type="submit" id="crear" class="btn btn-primary btn-block">Editar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <div class="card pt-4">
                <div class="cardCl card-header text-center text-white rounded">
                    <h3>Cambiar contraseña</h3>
                </div>
                <div class="card-body" style="box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">
                    <form id="edit_clave" enctype="multipart/form-data" action=<?php echo "index.php?pid=" . base64_encode("app/presentacion/permisoCliente.php") . "&priv=" . base64_encode(base64_encode("perfilCliente")) ?> method="post">
                        <input type="hidden" id="" value="<?php echo $_SESSION["id"] ?>">
                        <div class="form-group pt-3">
                            <input type="password" id="clave" value="" class="form-control" placeholder="Contraseña" required="required" autocomplete="off" minlength="4"  maxlength="15">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="newclave" value="" placeholder="Nueva Contraseña" required="required" autocomplete="off" minlength="4"  maxlength="15">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" value="" id="newclave2" placeholder="Repetir nueva contraseña" autocomplete="off" minlength="4"  maxlength="15">
                        </div>
                        <!-- <div class="form-group">
                            <input type="text" class="validanumericos form-control" value="" id="clave" placeholder="Clave" required="required" autocomplete="off">
                        </div> -->
                        <div class="form-group text-center">
                            <button type="submit" id="editarclave" class="btn btn-primary btn-block">Editar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function actualiza(nombre) { //Pasar nombre de imagen a input
        console.log(nombre);
        document.getElementById('m1').value = nombre;
    }
</script>
<script>
    $("#files").change(function() { //Cambiar de boton
        filename = this.files[0].name
        console.log(filename);
    });
</script>
<script>
    $(".read").on('keydown paste', function(e) { //Required de solo lectura
        e.preventDefault();
    });
</script>
