<div class="py-5 my-5"></div>
<div class="py-5 my-4"></div>
<footer class="col-12 bg-dark text-white mt-5 pt-4" id="myFooter">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                        <h3>Comentario</h3>
                        <form action="mailto:julian1918@hotmail.es" method="post" enctype="text/plain">
                            <div class="form-group">
                                <input type="text" maxlength="40" minlength="8" id="nombre1" class="form-control" placeholder="Nombre" required="required" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" maxlength="10" minlength="8" id="telefono1" class="form-control" placeholder="Telefono" required="required" autocomplete="off" pattern="[0-9]*" title="Formato numerico">
                            </div>
                            <div class="form-group">
                                <input type="text" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" id="correo1" class="form-control" placeholder="Correo" required="required" autocomplete="off" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" title="ejemplo@hotmail.com">
                            </div>
                            <div class="form-group">
                                <textarea name="comentario" minlength="15" class="form-control" placeholder="Comentario" required="required"></textarea>
                            </div>
                            <button class="btn btn-danger btn_get btn_get_two" type="submit">Enviar</button>
                        </form>
                </div>
                <div class="col-lg-4 col-md-6">
                    <h3>Contáctanos</h3>
                    <p><b>Dirección:</b></p>
                    <p class="pb-4">Cra 95 # 71-46 sur</p>
                    <p><b>Correo:</b></p>
                    <p class="pb-4">JDComics@hotmail.com</p>
                    <p><b>Telefono:</b></p>
                    <p>+57 3115413369</p>
                </div>
                <div class="col-lg-4 col-md-6 tex-white">
                    <h3>Redes sociales</h3>
                        <a href="#"><i class="fab fa-facebook fa-2x" style='color: white;'></i></a>
                        <a href="#"><i class="fab fa-twitter fa-2x" style='color: white;'></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-4">
        <div class="row">
            <div class="col-lg-12 col-sm-7">
                <p class="mb-0 f_400 text-center">© JD Comics.. 2021 Todos los derechos reservados.</p>
            </div>
        </div>
    </div>
</footer>
<script>
    window.addEventListener("resize", function(){
    footer = function(){ 
     /*el alto que tiene el navegador*/
     $alto_navegador= $(window).height();
     $ancho_navegador= $(window).width();
     /*el alto que tiene el contenido de la pagina*/
     $alto_documento= $(document).height(); 
     /*  aqui condicionamos si el alto del contenido 
      *  es mayor que
      *  el alto del navegador*/
      
     if ($alto_documento>$alto_navegador)
     {
         /* si es mayor es que tiene un contenido mas 
          * largo que el alto del navegador y entonces lo dejamos a relativo*/
         $("footer").css({"position":"relative"})
         console.log("relative");
     }
     else
     {
         /* si el alto del contenido es menor que el alto del navegador es que
          * tenemos espacio vacio y le mandamos abajo*/
          if (($alto_navegador/$ancho_navegador)>1.7) {
            $("footer").css({"position":"fixed"})
         console.log("fixed");
         }else{
            $("footer").css({"position":"fixed"})
            $("footer").css({"bottom":"0px"})
         console.log("fixed");
         }
     } 
     
 }
 footer();
});

</script>




