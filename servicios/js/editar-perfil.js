$(document).ready(function () {
    
    console.log("jquery editar perfil corriendo");

    cargar();

    $("#edit").submit(function (e) { 
        e.preventDefault();
        actualizar();
        
    });

    $("#edit_clave").submit(function (e) { 
        e.preventDefault();
        actualizarClave();
    });

});


function cargar(){
    var data=1;
    $.ajax({
        type: "post",
        url: "servicios/service_usuario/actualizarPerfil.php",
        data: {data},
       
        success: function (response) {

            var resultado = JSON.parse(response);
            // console.log(resultado);
            

            $("#nombre").val(resultado.nombre);
            $("#apellido").val(resultado.apellido);
            $("#correo").val(resultado.correo);

            if(typeof(resultado.direccion) != "undefined" && resultado.direccion !== null){
                $("#direccion").val(resultado.direccion);
                $("#telefono").val(resultado.telefono);

            }
            let tempalete="";
            tempalete+=`
            <img src="data:image/png;base64,${resultado.foto}" class="mx-auto d-block" style="width:50vh; max-width:170px;height:50vh;max-height:170px;position:relative;box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);" >
            `;
               
            $("#foto").empty();
            $("#foto").append(tempalete);
            $("#m1").trigger("reset");
           


        }
    });
}



function actualizar(){
     
    if($("#files").length) { 
        var file_data = $('#files').prop('files')[0];  
            }else
            var file_data=" ";
        //   console.log(file_data);
        
    
        var form_data = new FormData();
        form_data.append('foto', file_data);
        form_data.append('nombre',$("#nombre").val());
        form_data.append('apellido',$("#apellido").val());
        form_data.append('id',$("#id").val());
        form_data.append('rol',$("#rol").val());
        form_data.append('data',2);


        if($("#direccion").length) { 
          form_data.append('direccion',$("#direccion").val());
      }
      if($("#telefono").length) { 
        form_data.append('telefono',$("#telefono").val());
    }
        
           


                


        Swal.fire({
            title: "Estas seguro?",
            text: "¿Desea actualizar sus datos?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, añadirlo!",
            cancelButtonText: "Cancelar!",
          }).then((result) => {
      
            
      
      
            if (result.isConfirmed) {
      
                $.ajax({
                    url: "servicios/service_usuario/actualizarPerfil.php",
                    type: "post",
                    dataType: "html",
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(res){


                 

                  let respuesta= res;
                  console.log(respuesta);
                  if(respuesta!=-1)
                  {
                  
                    Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Sus datos han sido actualizados.',
                      showConfirmButton: false,
                    
                    })
                   
                    setTimeout(function(){ location.reload(); }, 1000);
                    cargar();
                    
                  }else{
                    Swal.fire(
                      "Operación cancelada!",
                      "Se ha producido un error.",
                      "error"

                    );
                  

                  }
                 
      
              }
              
              });
      
            }else{
               
            }
          });


}


function actualizarClave(){
    var form_data = new FormData();
    form_data.append('old_clave', $("#clave").val());
    form_data.append('new_clave',$("#newclave").val());
    form_data.append('new_clave2',$("#newclave2").val());
    form_data.append('data',3);

    Swal.fire({
        title: "Estas seguro?",
        text: "¿Desea cambiar su clave de acceso?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, añadirlo!",
        cancelButtonText: "Cancelar!",
      }).then((result) => {
      
      $.ajax({
        url: "servicios/service_usuario/actualizarPerfil.php",
        type: "post",
        dataType: "html",
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){




     
      //  console.log(respuesta);

      if(respuesta!=-1)
      {
        Swal.fire({
          position: 'center',
          icon: "success",
          title: "Su clave ha sido actualizada correctamente",
          showConfirmButton: true,
          // timer: 2500
        })
         
        cargar();
        $("#edit_clave").trigger("reset");
      }else{
        Swal.fire(
          "Operación cancelada!",
          "Se ha producido un error. Vuelva a intentarlo",
          "error"

        );
      
        $("#edit_clave").trigger("reset");
      }
     

  }
  
  });
  
});
    

   
}