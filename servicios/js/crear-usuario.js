


function guardarDatos(usuario){
    switch (usuario) {
        case "cliente":
            crearCliente();
            break;
    
    }
}




// funcion creacion cliente


function crearCliente(){
    var data= {
        "nombre":  window.btoa($("#nombre").val())  ,
        "apellido": window.btoa($("#apellido").val()),
        "correo": window.btoa($("#correo").val()),       
        "clave": window.btoa($("#clave").val()),
      
    };

   
    var meta={
        "confim-clave": window.btoa($("#cClave").val()),
        "confirm-correo": window.btoa($("#cCorreo").val()),
        "usuario": $("#usuario").val()
    }

    $.ajax({
        type: "post",
        url: "servicios/service_sesion/crear-cuenta.php",
        data: {data, meta},
        
        success: function (response) {
            notificadorCrearCliente(parseInt(response));
        }
    });   
}




// funciones de notificacion

function notificadorCrearCliente(estado){
    switch (estado) {
        case 1:
            notificacion(estado,"success","Cuenta creada exitosamente");
            $("#crear").trigger("reset");
            setTimeout(() => {
                location.href="index.php";
            }, 2000);
           
            break;
        case -1:
            notificacion(estado,"error","Error al crear la cuenta, intente de nuevo");
            
             break;
        case -2:
            notificacion(estado,"error","Correo registrado, intente de nuevo");

            
                break;
       
    }
}

function notificacion(estado,icon,mensaje){
if(estado>0){
    Swal.fire({
        position: 'top-end',
        icon: icon,
        title: mensaje,
        showConfirmButton: false,
        timer: 1500
      })

}else{
    Swal.fire({
        position: 'center',
        icon: icon,
        title: mensaje,
        showConfirmButton: true,
       
      })

}
   
}

