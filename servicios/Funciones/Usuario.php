<?php

require_once (dirname(__FILE__)."../../requisitos.php");
$roles=requisito("usuario");


// LISTAR USUARIO (funcional)
function listarUsuario($objeto,$busqueda){
    $sesion = new ControladorUsuario();
    $reporte=$sesion->listar($objeto,$busqueda);

    echo json_encode($reporte->getDatos());
}



// ACTUALIZAR CUENTA  (funcional)
function actualizarUsuario($data, $objeto){
    $sesion = new ControladorUsuario();
    $reporte=$sesion->actualizar($objeto,$data);
    return $reporte->getCodigo();
}


function cargaInformacion($objeto,$id){
    $sesion = new ControladorUsuario();
    if($objeto!="Cliente")
    $consulta= "nombre,apellido,correo,foto";
    else
    $consulta= "nombre,apellido,correo,foto,direccion,telefono";
    $report=$sesion->cargarInformacion($objeto,$id,$consulta);
    $value=$report->getDatos();
    $value->foto =base64_encode($value->foto);
    // if($report->getCodigo()>0)
    // {
     echo json_encode($report->getDatos());
   
  

        
    // }
    // else
    // echo $report->getCodigo();
}






function cargarData($data)
{
    if(isset($_POST["nombre"]  ) && !empty($_POST["nombre"])){
        $data["nombre"]=$_POST["nombre"];
    }
    
    if(isset($_POST["apellido"]  ) && !empty($_POST["apellido"])){
        $data["apellido"]=$_POST["apellido"];
    }

    
    if(isset($_POST["direccion"]  ) && !empty($_POST["direccion"])){
        $data["direccion"]=$_POST["direccion"];
    }

    if(isset($_POST["telefono"]  ) && !empty($_POST["telefono"])){
        $data["telefono"]=$_POST["telefono"];
    }

    // $data["telefono"]="3005216914";
    
    
    if (isset($_FILES['foto']  ) ) {
    
        $imagen= $_FILES['foto'];
        $tipo = $imagen['type'];
        $size =$imagen['size'];
        
        
        if($tipo == "image/jpeg" || $tipo == "image/png"){
        
        
            $hola= (file_get_contents($_FILES['foto']['tmp_name']));
    
           
            $data["foto"]=$hola;
    
            
          
        }
    
      

    } 
    return $data;
}


function actualizarSesion ($verificador,$data)
{
    if(!isset($_SESSION))
    session_start();

    // var_dump($data);
    // var_dump($verificador);
    if($verificador==1) 
    {
    if (isset($data["nombre"]) && !empty($data["nombre"]) )
    {
    $_SESSION["datos"]->nombre=   $data["nombre"];
    }

    if (isset($data["apellido"]) && !empty($data["apellido"]) )
    {
    $_SESSION["datos"]->apellido=  $data["apellido"];
    }
    if (isset($data["telefono"]) && !empty($data["telefono"]) )
    {
        $_SESSION["datos"]->telefono=  $data["telefono"];
    }

    if (isset($data["direccion"]) && !empty($data["direccion"]) )
    {
        $_SESSION["datos"]->direccion=  $data["direccion"];
    }


     if (!empty($data["foto"]) )
    {
        $_SESSION["datos"]->foto = base64_encode($data["foto"]);
     }else
     {
        $sesion = new ControladorUsuario();
        $objeto= $_SESSION["rol"];
        $id= $_SESSION["id"];
        

      
        $foto= $sesion->cargarInformacion($objeto,$id,"foto");
        $value=$foto->getDatos();
        $_SESSION["datos"]->foto = base64_encode($value->foto);
        
     }
     return 1;

     

 }else return (-1);
}

function cargar($roles){
    $objeto= $_SESSION["rol"];
    $id= $_SESSION["id"];
    cargaInformacion($objeto,$id);

}


function guardarInformacion($roles){
    $objeto= $_SESSION["rol"];
    $id= $_SESSION["id"];
    $equiparador=trim(base64_decode($roles[$objeto]));
    $data=[];
    $data[$equiparador]= $id;
    $data=cargarData($data);
    $verificador=actualizarUsuario($data,$objeto);
    echo actualizarSesion($verificador,$data);
}


function editarclave($roles){
    $sesion = new ControladorUsuario();

    $objeto= $_SESSION["rol"];
    $id= $_SESSION["id"];

    $report=$sesion->cargarInformacion($objeto,$id,"clave");
    $value=$report->getDatos();

    $verificador = password_verify($_POST["old_clave"], $value->clave) ? 1 : 0;
    $valida_clave=  strcmp($_POST["new_clave"],$_POST["new_clave2"])?0:1; 
   if($verificador && $valida_clave)
        {
             $equiparador=trim(base64_decode($roles[$objeto]));
            $data=[
                $equiparador=>$id,
                "clave"=>password_hash($_POST["new_clave"],PASSWORD_BCRYPT )
            ];
          $report2=$sesion->actualizar($objeto,$data);
          echo $report2->getCodigo();
        }
    else
    echo -1;

}