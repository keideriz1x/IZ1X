<?php

class EMensaje {

    const CORRECTO = "CORRECTO";
    const ERROR = "ERROR";
    const INSERCION_EXITOSA = "INSERSION_EXITOSA";
    const ACTUALIZACION_EXITOSA = "ACTUALIZACION_EXITOSA";
    const ELIMINACION_EXITOSA = "ELIMINACION_EXITOSA";
    const ERROR_INSERSION = "ERROR_INSERSION";
    const ERROR_ACTUALIZACION = "ERROR_ACTUALIZACION";
    const ERROR_ELIMINACION = "ERROR_ELIMINACION";
    const NO_HAY_REGISTROS = "NO_HAY_REGISTROS";
    const ERROR_CONEXION_BD = "ERROR_CONEXION_BD";
    const CORREO_CORRECTO ="CORREO_CORRECTO";
    const ERROR_CORREO ="ERROR_CORREO";
    const LOGIN_EXITOSO ="LOGIN_EXITOSO";
    const ERROR_LOGIN ="ERROR_LOGIN";
    const CUENTA_RESTRINGIDA="CUENTA_RESTRINGIDA";

    public static function getMensaje($codigo) {
        switch ($codigo) {
            case EMensaje::CORRECTO:
                return new Respuesta(1, "Se ha realizado la operación de manera correcta.");
            case EMensaje::INSERCION_EXITOSA:
                return new Respuesta(1, "Se ha insertado el registro con éxito.");
            case EMensaje::ACTUALIZACION_EXITOSA:
                return new Respuesta(1, "Se ha actualizado el registro con éxito.");
            case EMensaje::ERROR_INSERSION:
                return new Respuesta(-1, "Se ha producido un error al insertar el registro.");
            case EMensaje::ERROR:
                return new Respuesta(-1, "Se ha producido un error al realizar la operación.");
            case EMensaje::ERROR_ACTUALIZACION:
                return new Respuesta(-1, "Se ha producido un error al acutalizar el registro.");
            case EMensaje::NO_HAY_REGISTROS:
                return new Respuesta(0, "No hay registros.");
            case EMensaje::ERROR_CONEXION_BD:
                return new Respuesta(-1, "Error al conectar con la base de datos.");
            case EMensaje::ERROR_CORREO:
                return new Respuesta(-1,"El correo electronico  no esta vinculado");
            case EMensaje::CORREO_CORRECTO:
                return new Respuesta(1,"El correo electronico  esta vinculado");
            case EMensaje::LOGIN_EXITOSO:
                return new Respuesta(1,"Login validado correctamente");
            case EMensaje::ERROR_LOGIN:
                return new Respuesta(-1,"Error al intentar iniciar sesión");
            case EMensaje::CUENTA_RESTRINGIDA:
                return new Respuesta(-1,"Su cuenta se encuentra restringida");           

        }
    }

}