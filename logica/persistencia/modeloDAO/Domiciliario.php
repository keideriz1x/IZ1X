<?php
class Domiciliario extends ModeloGenerico{
    protected $idDomiciliario;
    protected $nombre;
    protected $apellido;
    protected $correo;
    protected $clave;
	protected $foto;
	protected $estado;
    // protected $estado;

    public function __construct($propiedades=null){
        parent:: __construct("domiciliario",Domiciliario::class. $propiedades);
    }

    


	function getIdDomiciliario() { 
 		return $this->idDomiciliario; 
	} 

	function setIdDomiciliario($idDomiciliario) {  
		$this->idDomiciliario = $idDomiciliario; 
	} 

	function getNombre() { 
 		return $this->nombre; 
	} 

	function setNombre($nombre) {  
		$this->nombre = $nombre; 
	} 

	function getApellido() { 
 		return $this->apellido; 
	} 

	function setApellido($apellido) {  
		$this->apellido = $apellido; 
	} 

	function getCorreo() { 
 		return $this->correo; 
	} 

	function setCorreo($correo) {  
		$this->correo = $correo; 
	} 

	function getClave() { 
 		return $this->clave; 
	} 

	function setClave($clave) {  
		$this->clave = $clave; 
	} 

	function getFoto() { 
 		return $this->foto; 
	} 

	function setFoto($foto) {  
		$this->foto = $foto; 
	} 

	function getEstado() { 
 		return $this->estado; 
	} 

	function setEstado($estado) {  
		$this->estado = $estado; 
	} 
}