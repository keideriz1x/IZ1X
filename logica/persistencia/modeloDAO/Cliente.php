<?php

class Cliente extends ModeloGenerico{
    protected $idCliente;
    protected $nombre;
    protected $apellido;
    protected $correo;
    protected $clave;
    protected $estado;
    protected $foto;
    protected $direccion;
    protected $telefono;

    public function __construct($propiedades=null){
        parent:: __construct("cliente",Cliente::class. $propiedades);
    }

	function getIdCliente() { 
 		return $this->idCliente; 
	} 

	function setIdCliente($idCliente) {  
		$this->idCliente = $idCliente; 
	} 

	function getNombre() { 
 		return $this->nombre; 
	} 

	function setNombre($nombre) {  
		$this->nombre = $nombre; 
	} 

	function getApellido() { 
 		return $this->apellido; 
	} 

	function setApellido($apellido) {  
		$this->apellido = $apellido; 
	} 

	function getCorreo() { 
 		return $this->correo; 
	} 

	function setCorreo($correo) {  
		$this->correo = $correo; 
	} 

	function getClave() { 
 		return $this->clave; 
	} 

	function setClave($clave) {  
		$this->clave = $clave; 
	} 

	function getEstado() { 
 		return $this->estado; 
	} 

	function setEstado($estado) {  
		$this->estado = $estado; 
	} 

	function getFoto() { 
 		return $this->foto; 
	} 

	function setFoto($foto) {  
		$this->foto = $foto; 
	} 

	function getDireccion() { 
 		return $this->direccion; 
	} 

	function setDireccion($direccion) {  
		$this->direccion = $direccion; 
	} 

	function getTelefono() { 
 		return $this->telefono; 
	} 

	function setTelefono($telefono) {  
		$this->telefono = $telefono; 
	} 
}