<?php

if (!isset($_SESSION)) {
	session_start();


	if (isset($_SESSION["error"])) {
		echo $_SESSION["error"];

		session_destroy();
	}
}

$pagSinSesion = array(
	"presentacion/client/clave.php",
	"presentacion/client/car.php",
	"presentacion/client/crear.php",
	"presentacion/client/permisoClient.php",
	"presentacion/admin/permisoAdmin.php",
	"presentacion/excepciones.php",
	"presentacion/sinSesion.php",

	//direccion del archivo que hace el logeo
	"servicios/service_sesion/logear.php",
	"servicios/service_sesion/logout.php",
	//funcion que se encarga del logeo y crear cuenta


);

$pagConSesion = array(
	"presentacion/client/car.php",
);

$pid = null;
if (isset($_GET["pid"])) {
	$pid = base64_decode($_GET["pid"]);
}

$priv = null;
if (isset($_GET["priv"])) {
	$priv = base64_decode($_GET["priv"]);
}
?>

<!doctype html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<link href="https://bootswatch.com/4/cyborg/bootstrap.css" rel="stylesheet" />
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
	<script src="https://pagination.js.org/dist/2.1.5/pagination.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<link rel="stylesheet" type="text/css" href="css/estilo.css">


	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css'>
	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.theme.default.min.css'>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js'></script>

	<title>IZ1X</title>
	<link rel="icon" type="image/png" href="img/logo.png">
</head>

<body class="contenedor" style="background-color: dark;" <?php echo 'style="' . ((isset($_SESSION["id"]) && $_SESSION["rol"] == "administrador") || (isset($_SESSION["id"]) && $_SESSION["rol"] == "domiciliario") ? 'background: linear-gradient(to bottom, gray, white);background-attachment:fixed;' : 'background-color: white;') . '"'; ?>>
	<?php
	if (isset($pid)) {
		if (isset($_SESSION["id"])) {
			if ($_SESSION["rol"] == "Administrador") {
				include "presentacion/admin/menuAdministrador.php";
			} else if ($_SESSION["rol"] == "Cliente") {
					include "presentacion/menuinicio.php";
				
			} else if ($_SESSION["rol"] == "Domiciliario") {
				include "presentacion/dom/menuDomiciliario.php";
			}
			include $pid;
		} else if (in_array($pid, $pagSinSesion)) {
			include $pid;
		} else {
			header("Location: index.php");
		}
	} else {
		include "presentacion/inicio.php";
	}


	if (isset($pid)) {
		if (isset($_SESSION["id"])) {
			if ($_SESSION["rol"] != "Administrador") {
				include "presentacion/footer.php";
			}
		} else {
			include "presentacion/footer.php";
		}
	} else {
		include "presentacion/footer.php";
	}



	?>




</body>

</html>
<script>
    $(document).ready(function() {

        var sync1 = $("#sync1");
        var sync2 = $("#sync2");
        var slidesPerPage = 5; 
        var syncedSecondary = true;

        sync1.owlCarousel({
            items: 1,
            slideSpeed: 2000,
            nav: true,
            autoplay: true,
            dots: true,
            loop: true,
            responsiveRefreshRate: 200,
            navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
        }).on('changed.owl.carousel', syncPosition);

        sync2
            .on('initialized.owl.carousel', function() {
                sync2.find(".owl-item").eq(0).addClass("current");
            })
            .owlCarousel({
                items: slidesPerPage,
                dots: false,
                nav: false,
                smartSpeed: 200,
                slideSpeed: 500,
                slideBy: slidesPerPage, 
                responsiveRefreshRate: 100
            }).on('changed.owl.carousel', syncPosition2);

        function syncPosition(el) {
            
            var count = el.item.count - 1;
            var current = Math.round(el.item.index - (el.item.count / 2) - .5);

            if (current < 0) {
                current = count;
            }
            if (current > count) {
                current = 0;
            }

            //end block

            sync2
                .find(".owl-item")
                .removeClass("current")
                .eq(current)
                .addClass("current");
            var onscreen = sync2.find('.owl-item.active').length - 1;
            var start = sync2.find('.owl-item.active').first().index();
            var end = sync2.find('.owl-item.active').last().index();

            if (current > end) {
                sync2.data('owl.carousel').to(current, 100, true);
            }
            if (current < start) {
                sync2.data('owl.carousel').to(current - onscreen, 100, true);
            }
        }

        function syncPosition2(el) {
            if (syncedSecondary) {
                var number = el.item.index;
                sync1.data('owl.carousel').to(number, 100, true);
            }
        }

        sync2.on("click", ".owl-item", function(e) {
            e.preventDefault();
            var number = $(this).index();
            sync1.data('owl.carousel').to(number, 300, true);
        });
    });
</script>